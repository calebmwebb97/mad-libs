package com.example.madlibs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_final_story.*
import java.lang.StringBuilder
import java.util.*

class FinalStoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_final_story)

        if (intent != null){
            val test = intent.getCharSequenceArrayExtra("elements")

            val storyName = intent.getStringExtra("file")

            val storyFile = when (storyName) {
                "university" -> Scanner(getResources().openRawResource(R.raw.university))
                "dr_sykes_welcome" -> Scanner(getResources().openRawResource(R.raw.dr_sykes_welcome))
                "dance" -> Scanner(getResources().openRawResource(R.raw.dance))
                "clothes" -> Scanner(getResources().openRawResource(R.raw.clothes))
                "tarzan" -> Scanner(getResources().openRawResource(R.raw.tarzan))
                else -> Scanner(getResources().openRawResource(R.raw.simple_story))
            }

            var storyString = StringBuilder()
            while (storyFile.hasNextLine()){
                storyString.append(storyFile.nextLine() + " ")
            }

            val pattern = "<(\\w|'|-)+>".toRegex()
            var thing = storyString.toString()
            test.forEach{ f ->
                thing = pattern.replaceFirst(thing, f.toString())
            }

            Story.text = thing
        }
    }
}
