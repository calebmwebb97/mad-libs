package com.example.madlibs

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_words.*
import java.lang.StringBuilder
import java.util.*

class WordsActivity : AppCompatActivity() {
    var count = 0
    val catList = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_words)

        if (intent != null) {
            val storyName = intent.getStringExtra("story")
            val storyFile = when (storyName) {
                "university" -> Scanner(getResources().openRawResource(R.raw.university))
                "dr_sykes_welcome" -> Scanner(getResources().openRawResource(R.raw.dr_sykes_welcome))
                "dance" -> Scanner(getResources().openRawResource(R.raw.dance))
                "clothes" -> Scanner(getResources().openRawResource(R.raw.clothes))
                "tarzan" -> Scanner(getResources().openRawResource(R.raw.tarzan))
                else -> Scanner(getResources().openRawResource(R.raw.simple_story))
            }
            val storyString = StringBuilder()
            while (storyFile.hasNextLine()){
                storyString.append(storyFile.nextLine())
            }
            val pattern = "<(\\w|'|-)+>".toRegex()
            val found = pattern.findAll(storyString)

            found.forEach { f ->
                val m = f.value
                catList.add(m)
            }
            TypeWordHere.hint = catList[0].substring(1, catList[0].length-2).split("-").joinToString(" ")
            Remaining.text = catList.size.toString() + " words remaining"
        }
    }

    //I need to control when I submit the word
    fun onSubmitClick(view: View){
        catList[count] = TypeWordHere.text.toString()
        TypeWordHere.text.clear()
        count++
        if(count < catList.size) {
            TypeWordHere.hint =
                catList[count].substring(1, catList[count].length - 2).split("-").joinToString(" ")
            Remaining.text = (catList.size-count).toString() + " word(s) remaining"
        }
        else
        {
            val myIntent = Intent(this, FinalStoryActivity::class.java)
            myIntent.putExtra("elements", catList.toTypedArray())
            myIntent.putExtra("file", intent.getStringExtra("story"))
            startActivity(myIntent)
        }
    }
}
