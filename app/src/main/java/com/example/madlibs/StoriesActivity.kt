package com.example.madlibs

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_stories.*
import java.util.*

class StoriesActivity : AppCompatActivity() {
    private val storyList = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stories)

        makeStoriesList()

        var myAdapter = ArrayAdapter<String>(
            this, android.R.layout.simple_list_item_1, storyList
        )
        StoriesListView.adapter = myAdapter

        StoriesListView.setOnItemClickListener { _, view, position, _ ->
            listClick(view, storyList[position])
            //println(storyList[position])
            myAdapter.notifyDataSetChanged()
        }

    }

    fun makeStoriesList(){
        val input = Scanner(getResources().openRawResource(R.raw.stories))
        var i = 0
        while (input.hasNextLine()) {
            val result = input.nextLine().trim()
            //println(result)
            storyList.add(result)
            i++
        }
    }

    fun listClick(view: View, string: String) {
        val myIntent = Intent(this, WordsActivity::class.java)
        myIntent.putExtra("story", string)
        startActivity(myIntent)
    }


}
